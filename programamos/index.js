 const express = require('express');
 const app = express();
 const jwt = require('jsonwebtoken');

 const informacion = {
     nombre: 'Dani',
     password: '123456',
     email: 'dani@acamica.com',
     telefono: '115823867',
     fecha_nacimiento: new Date('10/22/1986')
 }

 const firma = 'hola_mundo';

//firmar
const token = jwt.sign(informacion.nombre,informacion.password);
console.log(token);
console.log(informacion);

//descodificar
const decodificar = jwt.decode(token,informacion.password);
console.log(decodificar);

app.listen(3000,()=>{
    console.log('Server corriendo por el puerto 3000')
});